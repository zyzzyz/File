package com.zyz.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;



public class JdbcUtils {

	
	private static ComboPooledDataSource dataSource;
	private static Connection con;
	
	
	static {
		dataSource = new ComboPooledDataSource();
	}

	/**
	 * 
	 * @throws SQLException
	 */
	public static Connection getConnection() {

		try {
			con = dataSource.getConnection();
			

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return con;

	}

	/**
	 * 
	 * @param con
	 * @param stmt
	 * @param rs
	 */

	public static void closeAll(Connection con, PreparedStatement ps, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (con != null && !con.isClosed()) {
				con.close();
				con = null;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
