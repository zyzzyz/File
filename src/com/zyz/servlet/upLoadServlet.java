package com.zyz.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;
import com.zyz.entity.File1;
import com.zyz.entity.User;
import com.zyz.service.FileService;
import com.zyz.service.UserService;



public class upLoadServlet extends HttpServlet {

	UserService userService = new UserService();
	
	FileService fileService = new FileService();
	
	
	
	

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");

		 try{  
			 
		     SmartUpload smart=new SmartUpload();   
             smart.initialize(this.getServletConfig(), request, response);   
             smart.upload();   
            
             com.jspsmart.upload.File file1=smart.getFiles().getFile(0);
             
             String commonName=file1.getFileName();
             
             String ext =file1.getFileExt();
             String currentTime = System.currentTimeMillis()+"";
  			 String fileName = currentTime+"."+ext;
             String path = "/pic/"+fileName;
  			 
             file1.saveAs(path);
           
 			 HttpSession session=request.getSession();
 			 
 			 User user= (User) session.getAttribute("user");
 			
             File1 file = new File1();
             
             
             file.setFileName(fileName);
             file.setUserId(user.getId());
             file.setCommonName(commonName);
             file.setPath(path);
             fileService.addFile(file);
 			request.getRequestDispatcher("/uploadSuccess1.jsp").forward(request, response);

         }   
         catch(Exception e){   
         }   

		
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}

}
