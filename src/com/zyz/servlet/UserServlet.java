package com.zyz.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zyz.entity.User;
import com.zyz.service.UserService;

public class UserServlet extends HttpServlet {
	
	UserService userService = new UserService();

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		String method = request.getParameter("method");
		
		if(method.equals("add")){
			addUser(request,response);
			
		}
		if(method.equals("find")){
			findById(request,response);
		}
		if(method.equals("update")){
			update(request,response);
		}
		
		if(method.equals("delete")){
			delete(request,response);
		}

		if(method.equals("getAll")){
			findAll(request,response);
			
		}
	}


	private void findAll(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<User> listuser =userService.findAll();
			request.setAttribute("listuser", listuser);
			

			
			request.getRequestDispatcher("/listUser.jsp").forward(request, response);
		}catch (Exception e) {
		e.printStackTrace();
		}
		
	}


	private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id =Integer.parseInt(request.getParameter("id"));
		userService.delet(id);
		findAll(request,response);
	}


	private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer id =Integer.parseInt(request.getParameter("id"));
		String username=request.getParameter("username");
		String passwd=request.getParameter("passwd");
		
	User user1=new User();
		user1.setId(id);
		user1.setUsername(username);
		user1.setPasswd(passwd);
		
		userService.update(user1);
		
		findAll(request,response);
	}


	private void findById(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer id =Integer.parseInt(request.getParameter("id"));
		User user1=userService.findById(id);
		request.setAttribute("user", user1);
		request.getRequestDispatcher("/updateUser.jsp").forward(request, response);
		
	}


	private void addUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String username=request.getParameter("username");
		String passwd=request.getParameter("passwd");
		
		User user1=new User();
		
		user1.setUsername(username);
		user1.setPasswd(passwd);
		
		userService.addUser(user1);
		request.getRequestDispatcher("/all.jsp").forward(request, response);

		
	}


	
}

