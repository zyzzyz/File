package com.zyz.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zyz.entity.File1;
import com.zyz.entity.Share;
import com.zyz.entity.User;
import com.zyz.service.FileService;
import com.zyz.service.ShareService;

public class downLoadServlet extends HttpServlet {
	
	FileService fileService = new FileService();
	ShareService shareService = new ShareService();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ��ȡ������� ��ֲ�ͬ�Ĳ�������
		String method = request.getParameter("method");
		
		if ("downList".equals(method)) {
			// ���������б�
			downList(request,response);
		}
		
		else if ("down".equals(method)) {
			// ����
			down(request,response);
		}
		
		else if("delete".equals(method)){
			
			delete(request,response);
			
		} else if("share".equals(method)){
			share(request,response);
		}
		 else if("sharelist".equals(method)){
			 sharelist(request,response);
			}
		 else if("update".equals(method)){
			 updateFileName(request, response);
			}
		 else if("findById".equals(method)){
			findById(request, response);
			}
		
		
	}

	public void sharelist(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		List<Share> shareList = new ArrayList<Share>();
		shareList = shareService.queryShareFile();
		
		// 3. ���浽request��
		request.setAttribute("shareList", shareList);
		// 4. ת��
		request.getRequestDispatcher("/sharelist.jsp").forward(request, response);
		
		
	}

	public void updateFileName(HttpServletRequest request, HttpServletResponse response)
	
			throws ServletException, IOException {
		
		    request.setCharacterEncoding("UTF-8");
		    int fileId = Integer.parseInt(request.getParameter("fileId"));
	        String commonName = request.getParameter("commonName");
	        commonName = new String(commonName.getBytes("ISO8859-1"),"UTF-8");

	        fileService.updateFile(fileId,commonName);
	        shareService.updateShare(fileId, commonName);
			downList(request,response);


		
	}
	
	public void findById(HttpServletRequest request, HttpServletResponse response)
	
			throws ServletException, IOException {
		  Integer fileId = Integer.parseInt(request.getParameter("fileId"));
		   
		   File1 file=fileService.findById(fileId);
		   

			// 3. ���浽request��
			request.setAttribute("file", file);
			// 4. ת��
			request.getRequestDispatcher("/updateFile.jsp").forward(request, response);
			
			

		
	}
	
	public void share(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		Integer fileId = Integer.parseInt(request.getParameter("fileId"));
		
		String fileName = request.getParameter("fileName");
		fileName = new String(fileName.getBytes("ISO8859-1"),"UTF-8");
		
		String commonName = request.getParameter("commonName");
		commonName = new String(commonName.getBytes("ISO8859-1"),"UTF-8");
		String path = request.getParameter("path");
	
		
		shareService.addShareFile(fileId,fileName,commonName,path);
		downList(request,response);
		
	}

	public void delete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException  {
		
		Integer id =Integer.parseInt(request.getParameter("fileId"));
		fileService.delete(id);
		shareService.delete(id);
		downList(request,response);
		
		
	}

	public void downList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
				File1 file1 = new File1();
				
				HttpSession session=request.getSession();
	 			 
	 			User user= (User) session.getAttribute("user");
	 			
	 			file1.setUserId(user.getId());
	 		
				
				
				List<File1> listFile = fileService.queryFile(file1);
			
		
				// 3. ���浽request��
				request.setAttribute("listFile", listFile);
				// 4. ת��
				request.getRequestDispatcher("/downlist.jsp").forward(request, response);
				
				

			}

			
			/**
			 *  3. ��������
			 */
			public void down(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
				// ��ȡ�û����ص��ļ����(url��ַ��׷�����,get)
				String path = request.getParameter("path"); 
				String commonName = request.getParameter("commonName");
				path = new String(path.getBytes("ISO8859-1"),"UTF-8");
				commonName=new String(commonName.getBytes("ISO8859-1"),"UTF-8");
				// �Ȼ�ȡ�ϴ�Ŀ¼·��
				String basePath = getServletContext().getRealPath(path);
				// ��ȡһ���ļ���
				InputStream in = new FileInputStream(new File(basePath));
				
				// ����ļ��������ģ���Ҫ����url����
				commonName = URLEncoder.encode(commonName, "UTF-8");
				// �������ص���Ӧͷ
				response.setHeader("content-disposition", "attachment;fileName=" + commonName);
				
				// ��ȡresponse�ֽ���
				OutputStream out = response.getOutputStream();
				byte[] b = new byte[1024];
				int len = -1;
				while ((len = in.read(b)) != -1){
					out.write(b, 0, len);
				}
				// �ر�
				out.close();
				in.close();
				
				
			}
			
			public void doPost(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
				this.doGet(request, response);
			}

}
