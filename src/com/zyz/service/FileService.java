package com.zyz.service;

import java.util.List;

import com.zyz.dao.FileDao;
import com.zyz.entity.File1;

public class FileService {

	FileDao fileDao = new FileDao();
	
	public void addFile(File1 file){
		
		fileDao.addFile(file);
		}
	
	public void updateFile(int fileId ,String commonName){
		
		fileDao.update(fileId,commonName);
		}
	
	public void delete(int fileId){
		
		fileDao.delet(fileId);
		}
    public File1 findById(int fileId){
		
		return fileDao.findById(fileId);
		}
    
    public List<File1> queryFile(File1 file){
		
		List<File1> listFile =fileDao.findFile(file);
		
	
		return listFile;
		
		
		
		}
}
