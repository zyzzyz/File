package com.zyz.service;

import java.util.List;

import com.zyz.dao.UserDao;
import com.zyz.entity.User;

public class UserService {

	UserDao userDao = new UserDao();

	public User login(String username, String passwd) {

		boolean isLogin = false;

		return userDao.login(username, passwd);

	}

	public List<User> findAll() {

		return userDao.findAll();

	}

	public void addUser(User user) {

		userDao.addUser(user);

	}

	public void delet(int id) {

		userDao.delet(id);
	}

	public User findById(int id) {

		return userDao.findById(id);

	}

	public void update(User user) {

		userDao.update(user);

	}

}
