package com.zyz.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.zyz.entity.File1;
import com.zyz.entity.Share;
import com.zyz.entity.User;
import com.zyz.utils.JdbcUtils;



public class ShareDao {

	Connection conn = null;

	PreparedStatement ps=null;

	ResultSet rs = null;

	
	/**
	 * 查询全部分享的文件
	 * @return
	 */

	public List<Share> findShareFile() {

		List<Share> listFile = new ArrayList<Share>();

		try {
			conn = JdbcUtils.getConnection();

		
			
			String sql = "select*from share where 1=1 ";
			
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery(sql);
			while (rs.next()) {

				Share share = new Share();
				share.setFileId(rs.getInt("file_id"));
				share.setFileName(rs.getString("file_name"));
				share.setCommonName(rs.getString("common_name"));
				share.setPath(rs.getString("path"));
			
				listFile.add(share);

			}

			return listFile;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	
	/**
	 * 添加分享文件
	 * @param file
	 */
	public void addShareFile(int fileId,String fileName,String commonName,String path)  {

	

		try {
			conn = JdbcUtils.getConnection();

			String sql = "insert into share(file_id,file_name,common_name,path) values(?,?,?,?)";
			
			
			ps = conn.prepareStatement(sql);

			
			ps.setInt(1,fileId);
			ps.setString(2,fileName);
			ps.setString(3, commonName);
			ps.setString(4, path);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 删除分享文件
	 * @param id
	 */
	
	public void delet(int fileId)  {

	

		try {
			conn = JdbcUtils.getConnection();

			String sql = "delete from share where file_id=" + fileId;
			
			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	

	/**
	 * 修改文件名
	 * @param commonName
	 */
	
	public void update(int id,String commonName)  {

		

		try {
			conn = JdbcUtils.getConnection();

			String sql = "update share set common_name='" +commonName
					+ "'where file_id='"
					+ id+ "'";

			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}

	
}
