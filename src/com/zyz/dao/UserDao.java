package com.zyz.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.zyz.entity.User;
import com.zyz.utils.JdbcUtils;



public class UserDao {

	Connection conn = null;

	PreparedStatement ps=null;

	ResultSet rs = null;
/**
 * 登录
 * @param username
 * @param passwd
 * @return
 */
	public User login(String username, String passwd) {


		User user = new User();

		try {
			conn = JdbcUtils.getConnection();

			String sql = "SELECT * FROM user WHERE username='" + username
					+ "' and passwd='" + passwd + "'";
			
			ps = conn.prepareStatement(sql);

			rs = ps.executeQuery(sql);

			while (rs.next()) {
				
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPasswd(rs.getString("passwd"));
				return user;
			}

			return user;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}
		

	}
	
	/**
	 * 查询全部用户
	 * @return
	 */

	public List<User> findAll() {

		List<User> listuser = new ArrayList<User>();

		try {
			conn = JdbcUtils.getConnection();

			String sql = "select*from user ";
			
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery(sql);
			while (rs.next()) {

				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPasswd(rs.getString("passwd"));

				listuser.add(user);

			}

			return listuser;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 添加用户
	 * @param user
	 */
	public void addUser(User user)  {

	

		try {
			conn = JdbcUtils.getConnection();

			String sql = "insert into user(username,passwd) values(?,?)";
			
			
			ps = conn.prepareStatement(sql);

			ps.setString(1, user.getUsername());

			ps.setString(2, user.getPasswd());

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 删除用户
	 * @param id
	 */
	
	public void delet(int id)  {

	

		try {
			conn = JdbcUtils.getConnection();

			String sql = "delete from user where id=" + id;
			
			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 查找用户
	 * @param id
	 * @return
	 */
	public User findById(int id)  {

		User user = new User();

		try {
			conn = JdbcUtils.getConnection();

			String sql = "select * from user where id=" + id;
			
			
			ps = conn.prepareStatement(sql);

			rs=ps.executeQuery(sql);
			
			
			
			while (rs.next()) {
				
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPasswd(rs.getString("passwd"));

			}
			return user;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}
		

	}
	
	/**
	 * 修改用户
	 * @param user
	 */
	
	public void update(User user)  {

		

		try {
			conn = JdbcUtils.getConnection();

			String sql = "update user set username='" + user.getUsername()
					+ "', passwd='" + user.getPasswd() + "'where id='"
					+ user.getId() + "'";

			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}

}
