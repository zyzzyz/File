package com.zyz.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.zyz.entity.File1;
import com.zyz.entity.User;
import com.zyz.utils.JdbcUtils;



public class FileDao {

	Connection conn = null;

	PreparedStatement ps=null;

	ResultSet rs = null;

	
	/**
	 * 查询全部文件
	 * @return
	 */

	public List<File1> findFile(File1 file) {

		List<File1> listFile = new ArrayList<File1>();

		try {
			conn = JdbcUtils.getConnection();

		
			
			String sql = "select*from file where 1=1 and user_id ='"+file.getUserId()+"'";
			

			if(null!=file.getFileName() && file.getFileName().equals("")){
				
				sql+=" and file_name '%'"+file.getFileName()+"'%'";
				
			}
			
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery(sql);
			while (rs.next()) {

				File1 file1 = new File1();
				file1.setFileId(rs.getInt("file_id"));
				file1.setFileName(rs.getString("file_name"));
				file1.setCommonName(rs.getString("common_name"));
				file1.setPath(rs.getString("path"));
			
				listFile.add(file1);

			}

			return listFile;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	
	/**
	 * 添加文件
	 * @param file
	 */
	public void addFile(File1 file)  {

	

		try {
			conn = JdbcUtils.getConnection();

			String sql = "insert into file(file_name,user_id,common_name,path) values(?,?,?,?)";
			
			
			ps = conn.prepareStatement(sql);

			ps.setString(1, file.getFileName());

			ps.setInt(2, file.getUserId());
			
			ps.setString(3, file.getCommonName());
			
			ps.setString(4, file.getPath());

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 删除文件
	 * @param id
	 */
	
	public void delet(int fileId)  {

	

		try {
			conn = JdbcUtils.getConnection();

			String sql = "delete from file where file_id=" + fileId;
			
			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 查找文件
	 * @param id
	 * @return
	 */
	public File1 findById(int fileid)  {

		File1 file = new File1();

		try {
			conn = JdbcUtils.getConnection();

			String sql = "select * from file where file_id=" + fileid;
			
			
			ps = conn.prepareStatement(sql);

			rs=ps.executeQuery(sql);
			
			
			
			while (rs.next()) {
				
				
				file.setFileId(rs.getInt("file_id"));
				file.setFileName(rs.getString("file_name"));
				file.setUserId(rs.getInt("user_id"));
				file.setCommonName(rs.getString("common_name"));
				
				

			}
			
			return file;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}
		
	
		

	}
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	
	public void update(int fileId ,String commonName)  {

		

		try {
			conn = JdbcUtils.getConnection();

			String sql = "update file set common_name='" + commonName
					+ "'where file_id="+fileId;
					

			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}

}
