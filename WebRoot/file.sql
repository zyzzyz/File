/*
Navicat MySQL Data Transfer

Source Server         : 123456
Source Server Version : 50540
Source Host           : 127.0.0.1:3306
Source Database       : file

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2017-05-28 18:32:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) DEFAULT NULL,
  `common_name` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES ('10', '强力搜索器V0.13.exe', null, null, '11');
INSERT INTO `file` VALUES ('15', 'Java开发实战1200例 第1卷.pdf', null, null, '11');
INSERT INTO `file` VALUES ('16', '作品3-李念航-14软件二.zip', null, null, '11');
INSERT INTO `file` VALUES ('17', '练习.txt', null, null, '11');
INSERT INTO `file` VALUES ('18', '翻译.docx', null, null, '11');
INSERT INTO `file` VALUES ('19', '新建文本文档.txt', null, null, '11');
INSERT INTO `file` VALUES ('35', '1495944378668.docx', '作文.docx', '/pic/1495944378668.docx', '0');
INSERT INTO `file` VALUES ('36', '1495944401553.sql', '家肯德基.sql', '/pic/1495944401553.sql', '0');
INSERT INTO `file` VALUES ('37', '1495949901938.docx', '大家看到.docx', '/pic/1495949901938.docx', '0');
INSERT INTO `file` VALUES ('38', '1495962862401.docx', '打电话和.docx', '/pic/1495962862401.docx', '0');
INSERT INTO `file` VALUES ('40', 'test1.sql', null, null, '11');

-- ----------------------------
-- Table structure for share
-- ----------------------------
DROP TABLE IF EXISTS `share`;
CREATE TABLE `share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `common_name` varchar(50) NOT NULL,
  `path` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of share
-- ----------------------------
INSERT INTO `share` VALUES ('43', '36', '1495944401553.sql', '家肯德基.sql', '/pic/1495944401553.sql');
INSERT INTO `share` VALUES ('44', '37', '1495949901938.docx', '大家看到.docx', '/pic/1495949901938.docx');
INSERT INTO `share` VALUES ('45', '38', '1495962862401.docx', '打电话和.docx', '/pic/1495962862401.docx');
INSERT INTO `share` VALUES ('46', '36', '1495944401553.sql', '家肯德基.sql', '/pic/1495944401553.sql');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(11) NOT NULL,
  `passwd` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('11', 'admin', '789');
INSERT INTO `user` VALUES ('12', 'adminzzz', '789');
INSERT INTO `user` VALUES ('13', 'admin44', '789');
INSERT INTO `user` VALUES ('14', 'admin44', '78911');
INSERT INTO `user` VALUES ('19', 'admin', '789888');
INSERT INTO `user` VALUES ('29', 'admin9999', '789');
INSERT INTO `user` VALUES ('30', 'admin', '321545');
INSERT INTO `user` VALUES ('31', '69654', '325465');
INSERT INTO `user` VALUES ('32', 'admin', '789');
INSERT INTO `user` VALUES ('33', 'admin', '36');
INSERT INTO `user` VALUES ('34', 'admin', '123');
INSERT INTO `user` VALUES ('35', '789', '896654');
INSERT INTO `user` VALUES ('36', 'admin', '789554');
INSERT INTO `user` VALUES ('37', 'admin', '789554');
INSERT INTO `user` VALUES ('38', 'admin', '789554');
INSERT INTO `user` VALUES ('39', 'admin', '789554');
INSERT INTO `user` VALUES ('40', 'admin', '789554');
INSERT INTO `user` VALUES ('41', 'admin', '789554');
INSERT INTO `user` VALUES ('42', 'admin', '123456');
INSERT INTO `user` VALUES ('43', 'admin', '123456');
