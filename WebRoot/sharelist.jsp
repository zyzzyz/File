<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>分享列表</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	
	<style type="text/css">

th {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}

td {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
     
}
a:link {
 text-decoration: none;
}
table{
border-collapse:collapse;
}

</style>
	
  </head>
  
  <body>	
 
	<table border="1" align="center" width="905">
	
	  
		<tr>
			<th>序号</th>
			<th>文件名</th>
			<th>操作</th>
		</tr>
		<c:forEach var="share" items="${requestScope.shareList}" varStatus="vs" >
			<tr>
				<td>${vs.count }</td>
				<td>${share.commonName}</td>
				<td>
					<%--<a href="${pageContext.request.contextPath }/fileServlet?method=down&..">下载</a>--%>
					<!-- 构建一个地址  -->
					<c:url var="url" value="downLoadServlet">
						<c:param name="method" value="down"></c:param>
						<c:param name="path" value="${share.path}"></c:param>
						<c:param name="commonName" value="${share.commonName }"></c:param>
					</c:url>
					<!-- 使用上面地址 -->
					<a href="${url}">下载</a>
				</td>
			</tr>
		</c:forEach>
	</table>  		
  </body>
</html>
